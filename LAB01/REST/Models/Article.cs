﻿namespace REST.Models
{
    public class Article
    {
        public string Id { get; set; } = string.Empty;

        public string Title { get; set; } = string.Empty;

        public string Content { get; set; } = string.Empty;

        public string[] Photos { get; set; } = new string[] { };

        public override bool Equals(object obj)
        {
            var a = obj as Article;
            if (a == null) { return false; }
            return a.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}