﻿namespace REST.Models
{ 
    public class Photo
    {
        public string Id { get; set; } = string.Empty;

        public string Title { get; set; } = string.Empty;

        public string Url { get; set; } = string.Empty;

        public override bool Equals(object obj)
        {
            var a = obj as Photo;
            if (a == null) { return false; }
            return a.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}