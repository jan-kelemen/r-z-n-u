﻿using REST.Database;
using REST.Models;
using REST.Security;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace REST.Controllers
{
    [RoutePrefix("api/photos")]
    [BasicAuthentication]
    public class PhotosController : ApiController
    {
        /// <summary>
        /// Gets a list of all photos.
        /// </summary>
        /// <returns>
        /// <para>200 OK and a list of all photos in the body.</para>
        /// </returns>
        // GET api/photos - Get all photos
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Photo>))]
        [Route]
        public IHttpActionResult GetPhotos()
        {
            return Ok(DatabaseGateway.GetPhotos());
        }

        /// <summary>
        /// Gets a photo.
        /// </summary>
        /// <param name="id">Identifier of a photo.</param>
        /// <returns>
        /// <para>200 OK and a article in the body if photo exists.</para>
        /// <para>404 Not Found without a body if photo doesn't exist.</para>
        /// </returns>
        // GET api/photos/5 - Get photo
        [HttpGet]
        [ResponseType(typeof(Photo))]
        [Route("{id}")]
        public IHttpActionResult GetPhoto(string id)
        {
            try
            {
                return Ok(DatabaseGateway.GetPhoto(id));
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Replaces a photo with provided photo.
        /// </summary>
        /// <param name="id">Identifier of a photo.</param>
        /// <param name="photo">New photo.</param>
        /// <returns>
        /// <para>204 No content if photo replacement was successfull.</para>
        /// <para>404 Not found and a empty body if photo doesn't exist.</para>
        /// </returns>
        // PUT api/photos/5 - Replace photo
        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult ReplacePhoto(string id, [FromBody] Photo photo)
        {
            try
            {
                if (DatabaseGateway.ReplacePhoto(id, photo))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Creates a new photo.
        /// </summary>
        /// <param name="photo">New photo.</param>
        /// <returns>
        /// <para>201 Created with Location header set to the URI of the new photo and empty body.</para>
        /// </returns>
        // POST api/photos - Create new photo
        [HttpPost]
        [Route]
        public IHttpActionResult CreatePhoto([FromBody] Photo photo)
        {
            string location = DatabaseGateway.CreatePhoto(photo);
            return Created(location, photo);
        }

        /// <summary>
        /// Deletes all photos.
        /// </summary>
        /// <returns>
        /// <para>204 No content if photos were deleted successfully.</para>
        /// </returns>
        // DELETE api/photos - Delete all photos
        [HttpDelete]
        [Route]
        public IHttpActionResult DeletePhotos()
        {
            try
            {
                if (DatabaseGateway.DeletePhotos())
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes a photo.
        /// </summary>
        /// <param name="id">Identifier of a photo.</param>
        /// <returns>
        /// <para>204 No content if a photo was deleted successfully.</para>
        /// <para>404 Not found with an empty body if photo doesn't exits.</para>
        /// </returns>
        // DELETE api/photos/5 - Delete article
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult DeletePhoto(string id)
        {
            try
            {
                if (DatabaseGateway.DeletePhoto(id))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
