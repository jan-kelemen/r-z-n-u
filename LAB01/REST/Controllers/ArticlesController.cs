﻿using REST.Database;
using REST.Models;
using REST.Security;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace REST.Controllers
{
    [RoutePrefix("api/articles")]
    [BasicAuthentication]
    public class ArticlesController : ApiController
    {
        /// <summary>
        /// Gets a list of all articles.
        /// </summary>
        /// <returns>
        /// <para>200 OK and a list of all articles in the body.</para>
        /// </returns>
        // GET api/articles - Get all articles
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Article>))]
        [Route]
        public IHttpActionResult GetArticles()
        {
            return Ok(DatabaseGateway.GetArticles());
        }

        /// <summary>
        /// Gets a article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <returns>
        /// <para>200 OK and a article in the body if article exists.</para>
        /// <para>404 Not Found without a body if article doesn't exist.</para>
        /// </returns>
        // GET api/articles/5 - Get article
        [HttpGet]
        [ResponseType(typeof(Article))]
        [Route("{id}")]
        public IHttpActionResult GetArticle(string id)
        {
            try
            {
                return Ok(DatabaseGateway.GetArticle(id));
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Gets a list of all photos of a article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <returns>
        /// <para>200 OK and a list of all article photos in the body if the article exist.</para>
        /// <para>404 Not Found without a body if article doesn't exist.</para>
        /// </returns>
        // GET api/articles/5/photos - Get all article photos
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Photo>))]
        [Route("{id}/photos")]
        public IHttpActionResult GetArticlePhotos(string id)
        {
            try
            {
                return Ok(DatabaseGateway.GetArticlePhotos(id));
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Gets a photo from an article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <param name="photoId">Identifier of a photo.</param>
        /// <returns>
        /// <para>200 Ok and a photo in the body if the article and the photo exist.</para>
        /// <para>404 Not Found if the article or the photo in that article don't exist.</para>
        /// </returns>
        // GET api/article/5/photos/5 - Get article photo
        [HttpGet]
        [ResponseType(typeof(Photo))]
        [Route("{id}/photos/{photoId}")]
        public IHttpActionResult GetArticlePhoto(string id, string photoId)
        {
            try
            {
                return Ok(DatabaseGateway.GetArticlePhoto(id, photoId));
            }
            catch
            {
                return NotFound();
            }
        }


        /// <summary>
        /// Replaces a article with provided article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <param name="article">New article.</param>
        /// <returns>
        /// <para>204 No content if article replacement was successfull.</para>
        /// <para>404 Not found and a empty body if article doesn't exist.</para>
        /// </returns>
        // PUT api/articles/5 - Replace article 
        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult ReplaceArticle(string id, [FromBody] Article article)
        {
            try
            {
                if (DatabaseGateway.ReplaceArticle(id, article))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Replaces a photo of a article with provided photo.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <param name="photoId">Identifier of a photo.</param>
        /// <param name="photo">New photo.</param>
        /// <returns>
        /// <para>204 No content if photo replacement was successfull.</para>
        /// <para>404 Not found and a empty body if article or the photo don't exist.</para>
        /// </returns>
        // PUT api/articles/5/photos/5 - Replace article photo
        [HttpPut]
        [Route("{id}/photos/{photoId}")]
        public IHttpActionResult ReplaceArticlePhoto(string id, string photoId, [FromBody] Photo photo)
        {
            try
            {
                if (DatabaseGateway.ReplaceArticlePhoto(id, photoId, photo))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Creates a new article.
        /// </summary>
        /// <param name="article">New article.</param>
        /// <returns>
        /// <para>201 Created with Location header set to the URI of the new article and empty body.</para>
        /// </returns>
        // POST api/articles - Create new article
        [HttpPost]
        [Route]
        public IHttpActionResult CreateArticle([FromBody] Article article)
        {
            string location = DatabaseGateway.CreateArticle(article);
            return Created(location, article);
        }

        /// <summary>
        /// Adds a photo to the article.
        /// </summary>
        /// <param name="id">Identifier of the article.</param>
        /// <param name="photo">Photo to be added.</param>
        /// <returns>
        /// <para>201 Created with Location header set to the URI of the new photo.</para>
        /// </returns>
        // POST api/articles/5/photos - Create new photo in article
        [HttpPost]
        [Route("{id}/photos")]
        public IHttpActionResult CreateArticlePhoto(string id, [FromBody] Photo photo)
        {
            string location = DatabaseGateway.CreateArticlePhoto(id, photo);
            return Created(location, photo);
        }

        /// <summary>
        /// Deletes all articles.
        /// </summary>
        /// <returns>
        /// <para>204 No content if articles were deleted successfully.</para>
        /// </returns>
        // DELETE api/articles - Delete all articles
        [HttpDelete]
        [Route]
        public IHttpActionResult DeleteArticles()
        {
            try
            {
                if(DatabaseGateway.DeleteArticles())
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes a article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <returns>
        /// <para>204 No content if a article was deleted successfully.</para>
        /// <para>404 Not found with an empty body if article doesn't exits.</para>
        /// </returns>
        // DELETE api/articles/5 - Delete article
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult DeleteArticle(string id)
        {
            try
            {
                if (DatabaseGateway.DeleteArticle(id))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes all photos of a article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <returns>
        /// <para>204 No content if the article photos were deleted successfully.</para>
        /// <para>404 Not found with an empty body if article doesn't exits.</para>
        /// </returns>
        // DELETE api/articles/5/photos - Delete all article photos
        [HttpDelete]
        [Route("{id}/photos")]
        public IHttpActionResult DeleteArticlePhotos(string id)
        {
            try
            {
                if (DatabaseGateway.DeleteArticlePhotos(id))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes a photo from a article.
        /// </summary>
        /// <param name="id">Identifier of a article.</param>
        /// <param name="photoId">Identifier of the photo.</param>
        /// <returns>
        /// <para>204 No content if the article photo was deleted successfully.</para>
        /// <para>404 Not found with an empty body if article or the photo don't exits.</para>
        /// </returns>
        // DELETE api/articles/5/photos - Delete article photo
        [HttpDelete]
        [Route("{id}/photos/{photoId}")]
        public IHttpActionResult DeleteArticlePhoto(string id, string photoId)
        {
            try
            {
                if (DatabaseGateway.DeleteArticlePhoto(id, photoId))
                {
                    return ResponseMessage(Request.CreateResponse(System.Net.HttpStatusCode.NoContent));
                }
                else
                {
                    return InternalServerError();
                }
            }
            catch
            {
                return NotFound();
            }
        }
    }
}