﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace REST.Exceptions
{
    public class ArticleDoesntExistException : Exception
    {
        public ArticleDoesntExistException()
        {
        }

        public ArticleDoesntExistException(string message) : base(message)
        {
        }

        public ArticleDoesntExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ArticleDoesntExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}