﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace REST.Exceptions
{
    public class PhotoDoesntExistException : Exception
    {
        public PhotoDoesntExistException()
        {
        }

        public PhotoDoesntExistException(string message) : base(message)
        {
        }

        public PhotoDoesntExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PhotoDoesntExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}