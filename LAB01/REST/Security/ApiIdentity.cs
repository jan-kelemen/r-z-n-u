﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace REST.Security
{
    public class ApiIdentity : IIdentity
    {
        public string Name { get; set; }

        public string AuthenticationType => "Basic";

        public bool IsAuthenticated => true;
    }
}