﻿using REST.Exceptions;
using REST.Models;
using REST.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.Database
{
    static internal class DatabaseGateway
    {
        static Dictionary<string, Article> _articles = new Dictionary<string, Article>();

        static Dictionary<string, Photo> _photos = new Dictionary<string, Photo>();

        static Dictionary<Tuple<string, string>, ApiIdentity> _users = new Dictionary<Tuple<string, string>, ApiIdentity>();

        static DatabaseGateway()
        {
            _users[new Tuple<string, string>("Alan", "Ford")] = new ApiIdentity { Name = "Grupa TNT" };
        }


        public static IEnumerable<Article> GetArticles()
        {
            return _articles.Values;
        }

        public static Article GetArticle(string id)
        {
            var article = _articles[id];
            if(article == null) { throw new ArticleDoesntExistException(); }
            return article;
        }

        public static IEnumerable<Photo> GetArticlePhotos(string id)
        {
            var article = GetArticle(id);

            var rv = new List<Photo>();
            foreach (var p in article.Photos)
            {
                rv.Add(_photos[p]);
            }
            return rv;
        }

        public static Photo GetArticlePhoto(string id, string photoId)
        {
            var article = GetArticle(id);
            if(!article.Photos.Contains(photoId)) { throw new PhotoDoesntExistException(); }
            return GetPhoto(photoId);
        }

        public static bool ReplaceArticle(string id, Article article)
        {
            var local = GetArticle(id);
            local.Content = article.Content;
            local.Title = article.Title;
            local.Photos = article.Photos;
            return true;
        }

        public static bool ReplaceArticlePhoto(string id, string photoId, Photo photo)
        {
            GetArticlePhoto(id, photoId);
            ReplacePhoto(photoId, photo);
            return true;
        }

        public static string CreateArticle(Article article)
        {
            article.Id = Guid.NewGuid().ToString();
            _articles.Add(article.Id, article);
            return article.Id;
        }

        public static string CreateArticlePhoto(string id, Photo photo)
        {
            var article = GetArticle(id);
            var photoId = CreatePhoto(photo);
            article.Photos = article.Photos.Add(photoId);
            return photoId;
        }

        public static bool DeleteArticles()
        {
            _articles.Clear();
            return true;
        }

        public static bool DeleteArticle(string id)
        {
            GetArticle(id);
            return _articles.Remove(id);
        }

        public static bool DeleteArticlePhotos(string id)
        {
            var article = GetArticle(id);
            article.Photos = new string[] { };
            return true;
        }

        public static bool DeleteArticlePhoto(string id, string photoId)
        {
            GetArticlePhoto(id, photoId);
            return DeletePhoto(photoId);
        }

        public static IEnumerable<Photo> GetPhotos()
        {
            return _photos.Values;
        }

        public static Photo GetPhoto(string id)
        {
            var photo = _photos[id];
            if(photo == null) { throw new PhotoDoesntExistException(); }
            return photo;
        }

        public static bool ReplacePhoto(string id, Photo photo)
        {
            GetPhoto(id);
            photo.Id = id;
            _photos[id] = photo;
            return true;
        }

        public static string CreatePhoto(Photo photo)
        {
            photo.Id = Guid.NewGuid().ToString();
            _photos.Add(photo.Id, photo);
            return photo.Id;
        }

        public static bool DeletePhotos()
        {
            _photos.Clear();
            foreach (var article in _articles.Values)
            {
                article.Photos = new string[] { };
            }
            return true;
        }

        public static bool DeletePhoto(string id)
        {
            GetPhoto(id);
            foreach(var article in _articles.Values)
            {
                article.Photos = article.Photos.Where(p => p != id).ToArray();
            }
            return _photos.Remove(id);
        }

        public static ApiIdentity GetUser(string username, string password)
        {
            var tuple = new Tuple<string, string>(username, password);
            if(!_users.ContainsKey(tuple)) { return null; }
            return _users[tuple];
        }

        private static T[] Add<T>(this T[] target, params T[] items)
        {
            // Validate the parameters
            if (target == null)
            {
                target = new T[] { };
            }
            if (items == null)
            {
                items = new T[] { };
            }

            // Join the arrays
            T[] result = new T[target.Length + items.Length];
            target.CopyTo(result, 0);
            items.CopyTo(result, target.Length);
            return result;
        }
    }
}